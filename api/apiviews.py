#from django.shortcuts import render
#from django.views import generic
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from datetime import datetime, timedelta

from .models import Activity, Property, Survey
from .serializers import ActivitySerializer, ActivitySerializerFullData, PropertySerializer, SurveySerializer

# Create your views here.
##### APIView #####
### v2/

##### LISTAPIView #####

class SurveyListAPIView(APIView):
    def get(self, request):
        survey_data = Survey.objects.all()
        data = SurveySerializer(survey_data, many=True).data
        return Response(data)

##### DETAILAPIView #####

class SurveyDetailAPIView(APIView):
    def get(self, request, pk):
        survey_data = get_object_or_404(Survey, pk=pk)
        data = SurveySerializer(survey_data).data
        return Response(data)
    

##### LISTAPIView #####

class PropertyListAPIView(APIView):
    def get(self, request):
        property_data = Property.objects.all()
        data = PropertySerializer(property_data, many=True).data
        return Response(data)
    

##### DEATAILAPIView #####
    
class PropertyDetailAPIView(APIView):
    def get(self, request, pk):
        property_data = get_object_or_404(Property, pk=pk)
        data = PropertySerializer(property_data).data
        return Response(data)



##### LISTAPIView #####

### v2/activity_list ### Read Only
class ActivityListAPIView(APIView):
    def get(self, request):
        today = datetime.now()
        lessdays = timedelta(days=3)
        moredays = timedelta(days=14)
        lfilter = today - lessdays
        rfilter = today + moredays
        activity_data = Activity.objects.filter(schedule__gt=lfilter, schedule__lt=rfilter)
        data = ActivitySerializerFullData(activity_data, many=True).data
        return Response(data)


##### DETAILAPIView #####

### v2/activity_detail ### Read Only
class ActivityDetailAPIView(APIView):
    def get(self, request, pk):
        activity_data = get_object_or_404(Activity, pk=pk)
        data = ActivitySerializer(activity_data).data
        return Response(data)
        


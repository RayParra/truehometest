from rest_framework.routers import DefaultRouter

from django.urls import path

from api import views, apiviews, genapiviews


app_name = "api"

router = DefaultRouter()

router.register(r'v5/activities_sets', genapiviews.ActivityViewSet, basename='activities_sets')


urlpatterns = [
    path('v2/list_activity/', apiviews.ActivityListAPIView.as_view(), name="activity_list"),
    
    path('v3/list_activities/', genapiviews.ActivityListAPIView.as_view(), name="list_activities"),
    path('v3/list_activities_full_data/', genapiviews.ActivityListAPIViewFullData.as_view(), name="list_activities_full_data"),
    path('v3/list_properties/', genapiviews.PropertyListAPIView.as_view(), name="list_properties"),
    path('v3/list_surveys/', genapiviews.SurveyListAPIView.as_view(), name="list_surveys"),
    
    path('v3/create_activity/', genapiviews.ActivityCreateAPIView.as_view(), name="create_activity"),
    path('v3/retrieve_activity/<int:pk>/', genapiviews.ActivityRetrieveAPIView.as_view(), name="retrieve_activity"),
    path('v3/destroy_activity/<int:pk>/', genapiviews.ActivityDestroyAPIView.as_view(), name="destroy_activity"),
    path('v3/update_activity/<int:pk>/', genapiviews.ActivityUpdateAPIView.as_view(), name="update_activity"),
    
    path('v4/activities/', genapiviews.ActivityListCreateView.as_view(), name="activities"),
    path('v4/activities/<int:pk>/', genapiviews.ActivityRetrieveUpdateDestroyAPIView.as_view(), name="retrieve_update_activities"),
]

urlpatterns += router.urls


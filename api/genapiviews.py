#from django.shortcuts import render
#from django.views import generic
#from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from django.shortcuts import get_object_or_404

from .models import Activity, Property, Survey
from .serializers import ActivitySerializer, ActivitySerializerFullData, PropertySerializer, SurveySerializer

# Create your views here.
    

##### CREATE #####
### v3/activity_create ### Create Only
class ActivityCreateAPIView(generics.CreateAPIView):
    serializer_class = ActivitySerializer
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        s = Property.objects.filter(id=request.data["property_id"]).first()
        if serializer.is_valid():
            if s.status == 'active':
                serializer.save()
                return Response({"message": "Created Successfully"}, status=status.HTTP_201_CREATED)
            else:
                return Response({"message": "La propiedad asociada esta desactivada"}, status=status.HTTP_400_BAD_REQUEST)
           
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




##### LIST #####
### v3/list_activities/
class ActivityListAPIView(generics.ListAPIView):
    serializer_class = ActivitySerializer
    
    def get_queryset(self):
        return Activity.objects.filter(status="active")

## List Full Data
class ActivityListAPIViewFullData(generics.ListAPIView):
    serializer_class = ActivitySerializerFullData
    
    def get_queryset(self):
        return Activity.objects.all()



class PropertyListAPIView(generics.ListAPIView):
    serializer_class = PropertySerializer
    
    def get_queryset(self):
        return Property.objects.filter(status="active")


class SurveyListAPIView(generics.ListAPIView):
    serializer_class = SurveySerializer
    
    def get_queryset(self):
        return Survey.objects.all()
    


##### RETRIEVE #####

class ActivityRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ActivitySerializerFullData
    
    def get_queryset(self):
        return self.get_serializer().Meta.model.objects.all()



##### UPDATE ##### 


class ActivityUpdateAPIView(generics.UpdateAPIView):
    serializer_class = ActivitySerializer
    
    def get_queryset(self, pk):
        return self.get_serializer().Meta.model.objects.all().filter(id=pk).first()

    def patch(self, request, pk=None):
        if self.get_queryset(pk):
            activity_serializer = self.serializer_class(self.get_queryset(pk))
            return Response(activity_serializer.data, status=status.HTTP_200_OK)
        return Response({"message": "Activity not Found"}, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk=None):
        if self.get_queryset(pk):
            activity_serializer = self.serializer_class(self.get_queryset(pk), data=request.data)
            if activity_serializer.is_valid():
                activity_serializer.save()
                return Response(activity_serializer.data, status=status.HTTP_200_OK)      
        return Response(activity_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



##### DELETE ##### 
## Status Change

class ActivityDestroyAPIView(generics.DestroyAPIView):
    serializer_class = ActivitySerializer
    
    def get_queryset(self):
        return self.get_serializer().Meta.model.objects.all()
    
    def delete(self, request, pk=None):
        activity = self.get_queryset().filter(id=pk).first()
        if activity:
            activity.status = "inactive"
            activity.save()
            return Response({"message": "Activity Cancelled"}, status=status.HTTP_200_OK)
        return Response({"message": "Activity not Found"}, status=status.HTTP_400_BAD_REQUEST)
    
    
    
    
 ### v4/activities ### List and Create
class ActivityListCreateView(generics.ListCreateAPIView):
    serializer_class = ActivitySerializer
    queryset = ActivitySerializer.Meta.model.objects.filter(status="active")
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        s = Property.objects.filter(id=request.data["property_id"]).first()
        if serializer.is_valid():
            if s.status == 'active':
                serializer.save()
                return Response({"message": "Created Successfully"}, status=status.HTTP_201_CREATED)
            else:
                return Response({"message": "La propiedad asociada esta desactivada"}, status=status.HTTP_400_BAD_REQUEST)
           
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



 ### v4/activities ### List and Create
class ActivityRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ActivitySerializer
    
    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        else:
            return self.get_serializer().Meta.model.objects.filter(id=pk).first()
    
    def patch(self, request, pk=None):
        if self.get_queryset(pk):
            activity_serializer = self.serializer_class(self.get_queryset(pk))
            return Response(activity_serializer.data, status=status.HTTP_200_OK)
        return Response({"message": "Activity not Found"}, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk=None):
        if self.get_queryset(pk):
            activity_serializer = self.serializer_class(self.get_queryset(pk), data=request.data)
            if activity_serializer.is_valid():
                activity_serializer.save()
                return Response(activity_serializer.data, status=status.HTTP_200_OK)      
        return Response(activity_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk=None):
        activity = self.get_queryset().filter(id=pk).first()
        if activity:
            activity.status = "inactive"
            activity.save()
            return Response({"message": "Activity Cancelled"}, status=status.HTTP_200_OK)
        return Response({"message": "Activity not Found"}, status=status.HTTP_400_BAD_REQUEST)
    
    
    
    ##### VIEWSETS #####
    
class ActivityViewSet(viewsets.ModelViewSet):
    serializer_class = ActivitySerializer
        
    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        else:
            return self.get_serializer().Meta.model.objects.filter(id=pk).first()
    
    def list(self, request):
        queryset = self.get_queryset()
        print(queryset)
        activity_serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(activity_serializer.data, status=status.HTTP_200_OK)
        
        
    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        s = Property.objects.filter(id=request.data["property_id"]).first()
        if serializer.is_valid():
            if s.status == 'active':
                serializer.save()
                return Response({"message": "Created Successfully"}, status=status.HTTP_201_CREATED)
            else:
                return Response({"message": "La propiedad asociada esta desactivada"}, status=status.HTTP_400_BAD_REQUEST)
           
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def update(self, request, pk=None):
        if self.get_queryset(pk):
            activity_serializer = self.serializer_class(self.get_queryset(pk), data=request.data)
            if activity_serializer.is_valid():
                activity_serializer.save()
                return Response(activity_serializer.data, status=status.HTTP_200_OK)      
        return Response(activity_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def destroy(self, request, pk=None):
        activity = self.get_queryset().filter(id=pk).first()
        if activity:
            activity.status = "inactive"
            activity.save()
            return Response({"message": "Activity Cancelled"}, status=status.HTTP_200_OK)
        return Response({"message": "Activity not Found"}, status=status.HTTP_400_BAD_REQUEST)
